print("################### Start creating databases ###################");
db = db.getSiblingDB('slyorderdb');
db.createUser(
    {
      user: "slyusr",
      pwd: "sly123456",
      roles: [{role: "readWrite", db: "slyorderdb"}]
    }
);

db = db.getSiblingDB('slycatalogdb');
db.createUser(
    {
      user: "slyusr",
      pwd: "sly123456",
      roles: [{role: "readWrite", db: "slycatalogdb"}]
    }
)
print("################### End creating databases ###################");