package shop.velox.checkout_orchestration.converter;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.order.api.dto.AddressDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderEntryDto;
import shop.velox.order.api.dto.OrderStatus.*;

@Mapper
public interface OrderConverter {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "orderStatus", constant = "DRAFT")
    @Mapping(source = "addressDto", target = "shippingAddress")
    @Mapping(source = "addressDto", target = "billingAddress")
    @Mapping(source = "cartDto.total", target = "totalPrice")
    @Mapping(source = "cartDto.id", target = "cartId")
    @Mapping(source = "cartDto.customerId", target = "customerId")
    @Mapping(source = "orderEntryDtos", target = "entries")
    OrderDto convertAddressAndCartToOrder(@MappingTarget OrderDto orderDto, AddressDto addressDto, CartDto cartDto, List<OrderEntryDto> orderEntryDtos);

}
