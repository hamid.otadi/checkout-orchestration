package shop.velox.checkout_orchestration.converter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import shop.velox.cart_orchestration.api.dto.ItemDto;
import shop.velox.order.api.dto.OrderEntryDto;

@Mapper
public interface ItemConverter {

  @Mapping(source = "name", target = "articleName")
  OrderEntryDto convertItemToOrderEntry(ItemDto itemDto);

}
