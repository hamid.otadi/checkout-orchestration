package shop.velox.checkout_orchestration.service.impl;

import java.net.URI;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.checkout_orchestration.converter.AddressConverter;
import shop.velox.checkout_orchestration.converter.ItemConverter;
import shop.velox.checkout_orchestration.converter.OrderConverter;
import shop.velox.checkout_orchestration.service.CheckoutOrchestrationService;
import shop.velox.commons.mail.service.MailService;
import shop.velox.commons.mail.service.MessageBuilder;
import shop.velox.commons.rest.response.RestResponsePage;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.user.api.dto.UserDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderEntryDto;
import shop.velox.order.api.dto.AddressDto;

@Component
public class CheckoutOrchestrationServiceImpl implements CheckoutOrchestrationService {

  private static final Logger LOG = LoggerFactory.getLogger(CheckoutOrchestrationServiceImpl.class);

  private final RestTemplate restTemplate;
  private final ItemConverter itemConverter;
  private final AddressConverter addressConverter;
  private final OrderConverter orderConverter;
  private final MailService mailService;

  @Value("${user.url}")
  private String userUrl;

  @Value("${order.url}")
  private String orderUrl;

  @Value("${cartorchestration.url}")
  private String cartOrchestrationUrl;

  @Value("${velox.mail.orderconfirmation.subject}")
  private String mailSubject;

  @Value("${velox.mail.orderconfirmation.body}")
  private String mailBody;

  public CheckoutOrchestrationServiceImpl(@Autowired final RestTemplate restTemplate,  @Autowired final MailService mailService) {
    this.restTemplate = restTemplate;
    this.mailService = mailService;
    itemConverter = Mappers.getMapper(ItemConverter.class);
    addressConverter = Mappers.getMapper(AddressConverter.class);
    orderConverter = Mappers.getMapper(OrderConverter.class);
  }

  @Override
  public ResponseEntity<OrderDto> createOrder(final CartDto cartDto) {
    //Cart-Orchestration returns updated Cart (updated Availability, Price, Catalog information)
    LOG.debug("Cart id: {}", cartDto.getId());

    URI uri = UriComponentsBuilder.fromUriString(cartOrchestrationUrl)
    .path("/carts/")
    .path(cartDto.getId())
    .build()
    .toUri();
    ResponseEntity<CartDto> cartDtoResponseEntity = restTemplate.exchange(uri, HttpMethod.GET, null/*httpEntity*/, shop.velox.cart_orchestration.api.dto.CartDto.class);

    //The Cart information cannot be fetched, the creation of the Order is not completed
    if(!cartDtoResponseEntity.getStatusCode().is2xxSuccessful()){
      return new ResponseEntity<>(cartDtoResponseEntity.getStatusCode());
    }

    //Retrieve the User information from the User service
    uri = UriComponentsBuilder.fromUriString(userUrl)
    .path("/users/")
    .path(cartDto.getCustomerId())
    .build()
    .toUri();
    ResponseEntity<UserDto> userDto = restTemplate.exchange(uri, HttpMethod.GET, null/*httpEntity*/, UserDto.class);

    //Create the Order in Draft status
    OrderDto orderDto = new OrderDto();
    List<OrderEntryDto> orderEntryDtos = new ArrayList<>();
    cartDtoResponseEntity.getBody()
        .getItems()
        .stream()
        .forEach(itemDto -> {
          OrderEntryDto orderEntryDto = getItemConverter().convertItemToOrderEntry(itemDto);
          orderEntryDtos.add(orderEntryDto);
    });
    //TODO Consolidate on Unique Address DTO for all services
    AddressDto addressDto = null;
    if(userDto.getStatusCode().equals(HttpStatus.OK)){
      addressDto = getAddressConverter().convertUserAddressToOrderAddress(userDto.getBody().getAddressDto());
      orderDto.setUser(userDto.getBody());
    }
    // For now (v1) shipping and billing addresses are the same
    //Populate the Order with collected information
    orderDto = getOrderConverter().convertAddressAndCartToOrder(orderDto, addressDto, cartDtoResponseEntity.getBody(), orderEntryDtos);

    //forward the request to the Order service
    uri = UriComponentsBuilder.fromUriString(orderUrl)
    .path("/orders")
    .build()
    .toUri();
    ResponseEntity<OrderDto> createdOrder = restTemplate.postForEntity(uri, orderDto, OrderDto.class);

    LOG.info("Order has been created: {}", createdOrder.getBody());
    return createdOrder;
  }

  @Override
  public ResponseEntity<OrderDto> getOrder(final String customerId, final String orderId, final String cachedEtag) {
    //Retrieve the Order from the Order service
    URI uri = UriComponentsBuilder.fromUriString(orderUrl)
    .path("/orders/")
    .path(orderId)
    .build()
    .toUri();

    //Append received Etag to IF_NONE_MATCH and ETAG header in the GET request that is being forwarded to the Order Service
    //Adding these headers is necessary since the Order Service requires that they are present in the Order Service GET Order request
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.IF_NONE_MATCH, HttpHeaders.ETAG);
    headers.setIfNoneMatch(cachedEtag);
    headers.setETag(cachedEtag);
    HttpEntity<String> entity = new HttpEntity<>("body", headers);

    ResponseEntity<OrderDto> existingOrderDto = restTemplate.exchange(uri, HttpMethod.GET, entity, OrderDto.class);

    LOG.debug("retrieved order: {}", existingOrderDto);

     if(!existingOrderDto.getStatusCode().is2xxSuccessful()) {
        return new ResponseEntity<>(existingOrderDto.getStatusCode());
     }
     return existingOrderDto;
  }


  @Override
  public RestResponsePage<OrderDto> getAllOrders(Pageable pageable, OrderStatus filter) {
    //Retrieve All Orders from the Order service
    URI uri = UriComponentsBuilder.fromUriString(orderUrl)
    .path("/orders")
    .queryParam("page", pageable.getPageNumber())
    .queryParam("size", pageable.getPageSize())
    .queryParam("statusFilter", filter)
    .build()
    .toUri();

     ParameterizedTypeReference<RestResponsePage<OrderDto>> responseType = new ParameterizedTypeReference<>() {};
     return restTemplate.exchange(uri, HttpMethod.GET, null, responseType).getBody();
  }

  @Override
  public RestResponsePage<OrderDto> getAllOrdersByCustomerId(String customerId, Pageable pageable, OrderStatus filter) {
    //Retrieve All Orders from the Order service for specific customer
    URI uri = UriComponentsBuilder.fromUriString(orderUrl)
    .path("/orders")
    .queryParam("customerId", customerId)
    .queryParam("page", pageable.getPageNumber())
    .queryParam("size", pageable.getPageSize())
    .queryParam("statusFilter", filter)
    .build()
    .toUri();

     ParameterizedTypeReference<RestResponsePage<OrderDto>> responseType = new ParameterizedTypeReference<>() {};
     return restTemplate.exchange(uri, HttpMethod.GET, null, responseType).getBody();
  }

  @Override
  public ResponseEntity<OrderDto> updateOrder(final String customerId, final String orderId, final OrderDto order, final String cachedEtag) {
    //Update the Order in the Order service
    URI uri = UriComponentsBuilder.fromUriString(orderUrl)
    .path("/orders/")
    .path(orderId)
    .build()
    .toUri();

    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.IF_MATCH, HttpHeaders.ETAG);
    headers.setIfMatch(cachedEtag);
    headers.setETag(cachedEtag);
    HttpEntity<OrderDto> entity = new HttpEntity<>(order, headers);

    ResponseEntity<OrderDto> currentOrderDto = restTemplate.exchange(uri, HttpMethod.GET, entity, OrderDto.class);

    ResponseEntity<OrderDto> updatedOrderDto = restTemplate.exchange(uri, HttpMethod.PATCH, entity, OrderDto.class);

     LOG.debug("updated order: {}", updatedOrderDto);
     if(!updatedOrderDto.getStatusCode().is2xxSuccessful()) {
        return new ResponseEntity<>(updatedOrderDto.getStatusCode());
     }

     //if the order status has changed from DRAFT to ORDERED, i.e. the order is placed, the confirmation email is sent
     if(currentOrderDto.getBody().getOrderStatus() == OrderStatus.DRAFT && updatedOrderDto.getBody().getOrderStatus() == OrderStatus.ORDERED){
       //when the order is placed, delete the order's cart
       uri = UriComponentsBuilder.fromUriString(cartOrchestrationUrl)
           .path("/carts/")
           .path(order.getCartId())
           .build()
           .toUri();
       ResponseEntity<Void> deleteCartResponse = restTemplate.exchange(uri, HttpMethod.DELETE, null, Void.class);
       if(!deleteCartResponse.getStatusCode().is2xxSuccessful()){
          return new ResponseEntity<>(deleteCartResponse.getStatusCode());
       }

       //Retrieve the User information from the User service
       uri = UriComponentsBuilder.fromUriString(userUrl)
       .path("/users/")
       .path(updatedOrderDto.getBody().getCustomerId())
       .build()
       .toUri();
       ResponseEntity<UserDto> userDto = restTemplate.exchange(uri, HttpMethod.GET, null/*httpEntity*/, UserDto.class);

       //send confirmation email
       if(userDto.getBody().getEmail() != null){
         sendMessage(Objects.requireNonNull(userDto.getBody()).getEmail(), mailSubject, MessageFormat.format(mailBody, orderId));
       }
      }
     return updatedOrderDto;
  }

  protected ItemConverter getItemConverter() {
    return itemConverter;
  }

  protected AddressConverter getAddressConverter() { return addressConverter; }

  protected OrderConverter getOrderConverter() { return orderConverter; }

  protected void sendMessage(String recipient, String subject, String body){
    MessageBuilder builder = mailService.getSimpleMessageBuilder();
    builder
      .addRecipient(recipient)
      .setSubject(subject)
      .setMailBody(body)
      .send();
  }

}